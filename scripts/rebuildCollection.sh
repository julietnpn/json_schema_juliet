# first, build all overlays, as some schemas require overlays defined in definitios for other schemas and we don't want to keep track of dependencies
for file in definitions/*.js
  do echo $file; node $file overlaysOnly;
done
# once all overlays exists, build everything
for file in definitions/*.js
do node $file
done
