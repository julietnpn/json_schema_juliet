const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let terminationConventionSrc = JSON.parse( fs.readFileSync( `./../../../output/collection/conventions/log--activity--termination/object.json` ) );
let terminationConvention = new builder.ConventionSchema( terminationConventionSrc );

// reading the example
let terminationExample = exampleImporter(filename = "terminationLogSurvey_basic.json");

Object.keys(terminationConvention.schema.properties);

// empty object
let formattedExample = {
    termination_log: {},
    log_category: {},
    plant_asset:{},
    area_quantity:{},
    area_unit: {}
};

//the 1st entity in the convention is the plant asset, assigning that here
formattedExample.plant_asset = terminationExample[0];

//2nd entity is taxonomy_term--plant_type, this is missing

//3rd entity is taxonomy_term--season, this is missing

//4th is seeding log

//5th entity is log category
formattedExample.log_category = terminationExample[4];

//no area quantity or units
