const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let limeConventionSrc = JSON.parse( fs.readFileSync( `./../../../output/collection/conventions/log--input--lime/object.json` ) );
let limeConvention = new builder.ConventionSchema( limeConventionSrc );

// reading the example
let limeExample = exampleImporter(filename = "limeLogSurvey_basic.json");

Object.keys(limeConvention.schema.properties);

// empty object
let formattedExample = {
    lime_log: {},
    lime_quantity:{},
    lime_unit:{},
    plant_asset: {},
    area_quantity: {},
    percentage_unit:{},
    log_category: {},
    lime_type: {}
};

//the first entity in the convention is the lime quantity, assigning that here
formattedExample.lime_quantity = limeExample[0];

//second entity is the taxonomy_term--material_type
formattedExample.lime_type = limeExample[1];

//third entity is also taxonomy_term--material_type, just "lime", do we add this to lime_type?

//fourth entity is an input log
formattedExample.lime_log = limeExample[3];

//fifth entity is plant asset
formattedExample.plant_asset = limeExample[4];

//sixth entity is taxonomy term log category
formattedExample.log_category = limeExample[5];

//I'm not seeing area quantity or unit, or lime unit in the limeExample which I think is ok, they may not be required

// testing the example
limeConvention.validExamples = [ formattedExample ];

let test = limeConvention.testExamples();
