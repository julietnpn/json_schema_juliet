//I think this is the same as the basic example, might not need this one as well
const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let solarizationConventionSrc = JSON.parse( fs.readFileSync( `./../../../output/collection/conventions/log--activity--solarization/object.json` ) );
let solarizationConvention = new builder.ConventionSchema( solarizationConventionSrc );

// reading the example
let solarizationExample = exampleImporter(filename = "solarizationLogSurvey_complete.json");

Object.keys(solarizationConvention.schema.properties);

// empty object
let formattedExample = {
    solarization_log: {},
    plant_asset: {},
    area_quantity:{},
    area_unit:{},
    log_category: {}
};

//the 1st entity in the convention is the area quantity, assigning that here
formattedExample.area_quantity = solarizationExample[0];

//the 2nd entity in the convention is the area unit (%)
formattedExample.area_unit = solarizationExample[1];

//the 3rd entity in the convention is the solarization log
formattedExample.solarization_log = solarizationExample[2];

//the 4th entity in the convention is the log category
formattedExample.log_category = solarizationExample[3];

solarizationConvention.validExamples = [ formattedExample ];

let test = solarizationConvention.testExamples();
