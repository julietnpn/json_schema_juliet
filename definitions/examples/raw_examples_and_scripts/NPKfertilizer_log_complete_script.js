const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let NPKfertilizerConventionSrc = JSON.parse( fs.readFileSync( `./../../../output/collection/conventions/log--input--npk_fertilizer/object.json` ) );
let NPKfertilizerConvention = new builder.ConventionSchema( NPKfertilizerConventionSrc );

// reading the example
let NPKfertilizerExample = exampleImporter(filename = "NPKfertilizerLogSurvey_complete.json");

Object.keys(NPKfertilizerConvention.schema.properties);

// empty object
let formattedExample = {
    npk_fertilizer_log: {},
    plant_asset: {},
    area_quantity:{},
    percentage_unit:{},
    moisture: {},
    nitrogen:{},
    phosphorus: {},
    potassium:{},
    rate:{},
    rate_unit:{},
    log_category:{}
};

//the 1st entity in the convention is the area quantity, assigning that here
formattedExample.area_quantity = NPKfertilizerExample[0];

//2nd entity is the area unit (%)
formattedExample.area_unit = NPKfertilizerExample[1];

//3rd entity is a quantity--material
formattedExample.rate = NPKfertilizerExample[2];

//4th entity is a taxonomy_term--unit (cubic_ft), rate unit
formattedExample.rate_unit = NPKfertilizerExample[3];

//5th entity is taxonomy_term--material_type, name is alfalfa, idk what this is

//6th entity is quantity--standard n
formattedExample.nitrogen = NPKfertilizerExample[5];

//7th entity is quantity--standard %
formattedExample.percentage_unit = NPKfertilizerExample[6];

//8th entity is quantity--standard p
formattedExample.phosphorus = NPKfertilizerExample[7];

//9th entity is also %

//10th entity is quantity--standard k
formattedExample.potassium = NPKfertilizerExample[9];

//11th entity is also %

//12th entity is quantity--standard, ca??

//13th entity is the input log
formattedExample.npk_fertilizer_log = NPKfertilizerExample[12];

//14th entity is plant asset
formattedExample.plant_asset = NPKfertilizerExample[13];

//15th entity is log category
formattedExample.log_category = NPKfertilizerExample[14];

// testing the example
NPKfertilizerConvention.validExamples = [ formattedExample ];

let test = NPKfertilizerConvention.testExamples();
