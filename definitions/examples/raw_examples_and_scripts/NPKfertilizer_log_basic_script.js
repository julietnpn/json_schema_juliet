const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let NPKfertilizerConventionSrc = JSON.parse( fs.readFileSync( `./../../../output/collection/conventions/log--input--npk_fertilizer/object.json` ) );
let NPKfertilizerConvention = new builder.ConventionSchema( NPKfertilizerConventionSrc );

// reading the example
let NPKfertilizerExample = exampleImporter(filename = "NPKfertilizerLogSurvey_basic.json");

Object.keys(NPKfertilizerConvention.schema.properties);

// empty object
let formattedExample = {
    npk_fertilizer_log: {},
    plant_asset: {},
    area_quantity:{},
    percentage_unit:{},
    moisture: {},
    nitrogen:{},
    phosphorus: {},
    potassium:{},
    rate:{},
    rate_unit:{},
    log_category: {}
};

//the first entity in the convention is the area quantity, assigning that here
formattedExample.area_quantity = NPKfertilizerExample[0];

//second entity is the area unit (%)
formattedExample.area_unit = NPKfertilizerExample[1];

//third entity is the fertilizer log
formattedExample.npk_fertilizer_log = NPKfertilizerExample[2];

//fourth entity is plant asset
formattedExample.plant_asset = NPKfertilizerExample[3];

//fifth entity is log category
formattedExample.log_category = NPKfertilizerExample[4];

// testing the example
NPKfertilizerConvention.validExamples = [ formattedExample ];

let test = NPKfertilizerConvention.testExamples();

//plant asset errors
//error with quantities?
