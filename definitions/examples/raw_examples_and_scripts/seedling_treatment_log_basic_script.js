const builder = require(`../../../src/convention_builder`);
const { randomUUID } = require('crypto');
const { exampleImporter } = require("../../../src/schema_utilities.js");
const fs = require('fs');

// obtain the preexisting convention
let seedlingTreatmentConventionSrc = JSON.parse( fs.readFileSync( `./../../../output/collection/conventions/log--input--seedling_treatment/object.json` ) );
let seedlingTreatmentConvention = new builder.ConventionSchema( seedlingTreatmentConventionSrc );

// reading the example
let seedlingTreatmentExample = exampleImporter(filename = "seedlingTreatmentLogSurvey_basic.json");

Object.keys(seedlingTreatmentConvention.schema.properties);

// empty object
let formattedExample = {
    seedling_treatment_log: {},
    plant_asset: {},
    log_category:{}
};

//the first entity in the convention is the plant asset assigning that here
formattedExample.plant_asset = seedlingTreatmentExample[0];

//second entity is taxonomy_term--plant_type, this is missing from convention

//third entity is taxonomy_term--season, this is missing from convention

//fourth entity is log--seeding, this is missing

//fifth entity is log category
formattedExample.log_category = seedlingTreatmentExample[4];
