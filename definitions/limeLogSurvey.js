// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 39, Lime
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

//UUIDS
let limeLogUUID = randomUUID();
let limeQuantityUUID = randomUUID();
let limeUnitUUID = randomUUID();
let conventionUUID = randomUUID();
let areaPercentageUUID = randomUUID();
let percentageUnitUUID = randomUUID();
let limeTypeUUID = randomUUID();

//Main entity
////Example
let limeLogExample = {
    id:limeLogUUID,
    attributes: {
        name: "example lime log",
        status:"done",
    }
};
let limeLogError = {
    id:limeLogUUID,
    attributes: {
        name: "example lime log",
        status:"false",
    }
};
////Overlays and constants
let limeLog = new builder.SchemaOverlay({
    typeAndBundle: 'log--input',
    name: 'lime',
    validExamples: [limeLogExample],
    erroredExamples: [limeLogError]
});

//LOOK add description
limeLog.setMainDescription("Lime logs are used to record a lime application.");

limeLog.setConstant({
    attribute:"status",
    value:"done"
});

//lime quantity
////Examples
let limeQuantityExample = {
    id:limeQuantityUUID,
    attributes: {
        label: "lime",
        measure:"value"
    }
};
let limeQuantityError = {
    id:limeQuantityUUID,
    attributes: {
        label: "compost",
        measure:"area",
    }
};
////Overlays and constants
let limeQuantity = new builder.SchemaOverlay({
    typeAndBundle: "quantity--material",
    name: "lime",
    validExamples: [ limeQuantityExample ],
    erroredExamples: [ limeQuantityError ]
});
//LOOK set description
limeQuantity.setMainDescription("This represents the total quantity of lime applied. This quantity is always pushed even if empty in the survey.");
//LOOK what should the measure be here?
limeQuantity.setConstant({
    attribute: "measure",
    value:"value"
});
limeQuantity.setConstant({
    attribute: "label",
    value:"lime"
});

//Lime quantity units
////Examples
let limeUnitExample = {
    id:limeUnitUUID,
    attributes: {
        name: "lbs_acres",
    }
};
let limeUnitError = {
    id:limeUnitUUID,
    attributes: {
        name: "ft",
    }
};
////Overlays and constants
let limeUnit = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "lime unit",
    validExamples: [ limeUnitExample ],
    erroredExamples: [ limeUnitError ]
});
limeUnit.setEnum({
    attribute: "name",
    valuesArray: [
        "lbs_acres",
        "tons_acre",
        "kg_ha",
        "tons_ha"
    ],
    description: "Lime units are recorded in weight per area."
});
//LOOK set description - Adie
limeUnit.setMainDescription("")

let limeType = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--material_type",
    name: "lime type",
    validExamples: [],
    erroredExamples: []
});
limeType.setMainDescription("The type or brand of lime applied is stored here.");

limeType.setEnum({
    attribute: "name",
    valuesArray: [
        "calcitic_lime",
        "crushed_lime",
        "dolomitic_lime",
        "high_cal",
        "nutralime_op_hi_cal",
        "nutralime_op_hi_mag",
        "pelletized_lime",
        "supercal_98g"
    ],
    description: "The type or brand name of the lime applied. SurveyStack has an ontology list of these types."
});

//Convention
let limeConvention = new builder.ConventionSchema({
    title: "Lime Input",
    version: "0.0.1",
    schemaName:"log--input--lime",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    //LOOK add description
    description:`
    ##Purpose\n
    Lime logs are created to record a lime input to a field or planting.\n
    ##Specification\n
    text\n`,
    validExamples: [],
    erroredExamples: []
});

//add attributes
limeConvention.addAttribute( { schemaOverlayObject:limeLog, attributeName: "lime_log", required: true } );
limeConvention.addAttribute( { schemaOverlayObject:limeQuantity, attributeName: "lime_quantity", required: false } );
limeConvention.addAttribute( { schemaOverlayObject:limeUnit, attributeName: "lime_unit", required: false } );
limeConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "plant_asset", required: true } );
limeConvention.addAttribute({ schemaOverlayObject:"quantity--standard--area_percentage", attributeName: "area_quantity", required: false});
limeConvention.addAttribute({ schemaOverlayObject:"taxonomy_term--unit--%", attributeName: "percentage_unit", required: false});
limeConvention.addAttribute( { schemaOverlayObject:"taxonomy_term--log_category--amendment", attributeName: "log_category", required: false } );
limeConvention.addAttribute( { schemaOverlayObject: limeType, attributeName: "lime_type", required: false});

//add relationships
limeConvention.addRelationship( { containerEntity:"lime_log", relationName:"asset", mentionedEntity:"plant_asset", required: true } );
limeConvention.addRelationship( { containerEntity:"lime_log" , relationName:"quantity" , mentionedEntity:"area_quantity" , required: false } );
limeConvention.addRelationship( { containerEntity:"area_quantity" , relationName:"units" , mentionedEntity:"percentage_unit" , required: false } );
limeConvention.addRelationship( { containerEntity:"lime_log" , relationName:"quantity" , mentionedEntity:"lime_quantity" , required: false } );
limeConvention.addRelationship( { containerEntity:"lime_quantity" , relationName:"units" , mentionedEntity:"lime_unit" , required: false } );
limeConvention.addRelationship( { containerEntity:"lime_log" , relationName:"category" , mentionedEntity:"log_category" , required: true } );
limeConvention.addRelationship( { containerEntity:"lime_quantity", relationName:"material_type" , mentionedEntity:"lime_type" , required: true } );


let plantAssetExampleAttributes = limeConvention.overlays.plant_asset.validExamples[0].attributes;
let plantAssetUUID = limeConvention.overlays.plant_asset.validExamples[0].id;

let logCategoryExampleAttributes = limeConvention.overlays.log_category.validExamples[0].attributes;
let logCategoryUUID = limeConvention.overlays.log_category.validExamples[0].id;

let limeConventionExample = {
        id: conventionUUID,
        type: "Object",
        plant_asset: {
            id: plantAssetUUID,
            attributes: plantAssetExampleAttributes
        },
        lime_log: {
            id: limeLogUUID,
            attributes: {
                name: "example lime log",
                status: "done",
            },
            relationships: {
                asset: { data: [
                    {
                        type: "asset--plant",
                        id: plantAssetUUID
                    }
                ]},
                category: { data: [
                    {
                        type: "taxonomy_term--log_category",
                        id: logCategoryUUID
                    }
                ]},
                quantity: { data: [
                    {
                        type: "quantity--standard",
                        id: areaPercentageUUID
                    },
                    {
                        type: "quantity--material",
                        id: limeQuantityUUID
                    }
                ] }
            },
        },
        area_quantity: {
            id: areaPercentageUUID,
            attributes: {
                label: "area"
            },
            relationships: {
                units: { data: [
                    {
                        type:"taxonomy_term--unit",
                        id: percentageUnitUUID
                    }
                ] }
            }
        },
        area_unit:  {
            id: percentageUnitUUID,
            attributes: {
                name: "%"
            },   
        },
        lime_quantity: {
            id: limeQuantityUUID,
            attributes: {
                label: "lime",
                measure:"value"
            },
            relationships: {
                units: { data: [
                    {
                        type:"taxonomy_term--unit",
                        id: limeUnitUUID
                    }
                ] },
                material_type: { data: [
                    {
                        type:"taxonomy_term--material_type",
                        id: limeTypeUUID
                    }
                ] }
            }
        },
        lime_unit:  {
            id: limeUnitUUID,
            attributes: {
                name: "lbs_acres"
            },   
        },
        lime_type:  {
            id: limeTypeUUID,
            attributes: {
                name: "calcitic_lime"
            },   
        },
        log_category:
        {
            id: logCategoryUUID,
            attributes: logCategoryExampleAttributes
        }
};
let limeConventionError = {
    id: conventionUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    lime_log: {
        id: limeLogUUID,
        attributes: {
            name: "example lime log",
            status: "done",
        },
        relationships: {
            asset: { data: [
                {
                    type: "asset--plant",
                    id: plantAssetUUID
                }
            ]},
            quantity: { data: [
                // {
                //     type: "quantity--standard",
                //     id: areaPercentageUUID
                // },
                // // intentional error: a relationship is missing.
                // {
                //     type: "quantity--material",
                //     another: 1,
                //     // id: limeQuantityUUID
                //     id: "aaaaaa"
                // }
            ] }
        },
    },
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    area_unit:  {
        id: percentageUnitUUID,
        attributes: {
            name: "%"
        },
    }
};

limeConvention.validExamples = [limeConventionExample];
limeConvention.erroredExamples = [limeConventionError];

let test = limeConvention.testExamples();
let storageOperation = limeConvention.store();
