// Survey: https://app.surveystack.io/surveys/6346c1e467f839000121503e/edit
// Section: 22, pesticide, herbicide, fungicide
const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');
const fs = require('fs');

//UUIDs
let herbicideLogUUID = randomUUID();
let herbicideRateUUID = randomUUID();
let rateUnitUUID = randomUUID();
let conventionUUID = randomUUID();
let areaPercentageUUID = randomUUID();
let percentageUnitUUID = randomUUID();
let logCategoryUUID = randomUUID();
let herbicideNameUUID = randomUUID();

//Main entity
////Examples
let herbicideLogExample = {
    id:herbicideLogUUID,
    attributes: {
        name: "example herbicide log",
        status:"done",
    }
};
let herbicideLogError = {
    id:herbicideLogUUID,
    attributes: {
        name: "example herbicide log",
        status:"pending",
    }
};
////overlays and constants
let herbicideLog = new builder.SchemaOverlay({
    typeAndBundle: 'log--input',
    name: 'herbicide_pesticide',
    validExamples: [herbicideLogExample],
    erroredExamples: [herbicideLogError]    
});
herbicideLog.setMainDescription("This log records herbicide, pesticide and fungicide inputs.");

herbicideLog.setConstant({
    attribute:"status",
    value:"done",
    description: "The status should always be set to done to inherit the area."
});

let logCategoryExample = {
    id: logCategoryUUID,
    attributes: {
        name: "weed_control"
    }
};
let logCategoryError = {
    id: logCategoryUUID,
    attributes: {
        label: "seeding"
    }
};
////overlays and constants
let logCategory = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--log_category",
    name: "",
    validExamples: [logCategoryExample],
    erroredExamples: [logCategoryError],
});

logCategory.setEnum({
    attribute: "name",
    valuesArray: [
        "pest_disease_control",
        "weed_control"
    ],
    description: "This log is used to record pest disease control applications or inputs related to weed control."
});


//quantity - rate
////Examples
let herbicideRateExample = {
    id: herbicideRateUUID,
    attributes: {
        label: "application_rate",
        measure: "rate"
    }
};
////overlays and constants
let herbicideRate = new builder.SchemaOverlay({
    typeAndBundle: "quantity--material",
    name: "rate",
    validExamples: [ herbicideRateExample ]
});
herbicideRate.setMainDescription("The herbicide rate indicates the amount of product used for every surface unit.");
herbicideRate.setConstant({
    attribute: "measure",
    value:"rate"
});

//taxonomy term - units
////Examples
let herbicideRateUnitExample = {
    id: rateUnitUUID,
    name: "oz_acre"
};
let herbicideRateUnitError = {
    id: rateUnitUUID,
    name: "ounces_per_acre"
};
////overlays and constants
let herbicideRateUnit = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--unit",
    name: "rate_unit",
    validExamples: [ herbicideRateUnitExample ],
    erroredExamples: [ herbicideRateUnitError ]
});
herbicideRateUnit.setMainDescription("The units refer to the application amount in a mass to area ratio.")

//LOOK from taxonomy list in surveystack beta management question 22
//rateUnit.setEnum({
//    attribute: "name",
//    valuesArray: [
//
//    ],
//    description: "Several units are available, both imperial and metric. They are all compatible mass to area ratios."
//});

let herbicideName = new builder.SchemaOverlay({
    typeAndBundle: "taxonomy_term--material_type",
    name: "herbicide",
    validExamples: [],
    erroredExamples: []
});
herbicideName.setMainDescription("The name of the herbicide is stored here.");

//LOOK from taxonomy list in surveystack beta management question 22
herbicideName.setEnum({
    attribute: "name",
    valuesArray: [
      "aero"
    ],
    description: "This product name information is stored in a SurveyStack ontology list of herbicides, insecticides and fungicides."
});

//taxonomy term - active ingredient percent
let activeIngredient = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "active ingredient percent",
    //LOOK do we need new examples here, could we use the examples from herbicide rate?
    //validExamples: [ activeIngredientExample ],
    //erroredExamples: [ activeIngredientError ]
});
//LOOK - what should the value be here??
activeIngredient.setConstant({
    attribute: "measure",
    value:"rate"
});
//LOOK set description - Adie
activeIngredient.setMainDescription("The % of the active ingredient in the herbicide or pesticide product.");

//Convention
// Object
let herbicideConvention = new builder.ConventionSchema({
    title: "Herbicide or pesticide log",
    version: "0.0.1",
    schemaName:"log--input--herbicide",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:`
    ##Purpose\n
    Herbicide input log to be applied to a plant asset only if the farmer indicated so in their planting records. In other words, this log needs to be assigned to a planting.\n
    ##Specification\n
    text\n`,
    validExamples: [],
    erroredExamples: []
});

////add attributes
herbicideConvention.addAttribute( { schemaOverlayObject:herbicideLog, attributeName: "herbicide_log", required: true } );
herbicideConvention.addAttribute( { schemaOverlayObject:"asset--plant--planting", attributeName: "plant_asset", required: true } );
herbicideConvention.addAttribute( { schemaOverlayObject: "quantity--standard--area_percentage", attributeName: "area_quantity", required: false});
herbicideConvention.addAttribute( { schemaOverlayObject: "taxonomy_term--unit--%", attributeName: "area_unit", required: false});
//LOOK we always push quantity here even if there is no quantity specified, to ensure that the type or name information is stored in the material reference of the quantity
herbicideConvention.addAttribute( { schemaOverlayObject: herbicideRate, attributeName: "rate", required: true});
herbicideConvention.addAttribute( { schemaOverlayObject: herbicideRateUnit, attributeName: "rate_unit", required: true});
herbicideConvention.addAttribute( { schemaOverlayObject: herbicideName, attributeName: "herbicide_name", required: true});
herbicideConvention.addAttribute( { schemaOverlayObject:logCategory, attributeName: "log_category", required: true } );

////add relationships
herbicideConvention.addRelationship( { containerEntity:"herbicide_log", relationName:"asset", mentionedEntity:"plant_asset", required: true } );
herbicideConvention.addRelationship( { containerEntity:"herbicide_log" , relationName:"quantity" , mentionedEntity:"area_quantity" , required: false } );
herbicideConvention.addRelationship( { containerEntity:"area_quantity" , relationName:"units" , mentionedEntity:"area_unit" , required: false } );
herbicideConvention.addRelationship( { containerEntity:"herbicide_log" , relationName:"quantity" , mentionedEntity:"rate" , required: false } );
herbicideConvention.addRelationship( { containerEntity:"rate" , relationName:"units" , mentionedEntity:"rate_unit" , required: false } );
herbicideConvention.addRelationship( { containerEntity:"herbicide_log" , relationName:"category" , mentionedEntity:"log_category" , required: true } );
herbicideConvention.addRelationship( { containerEntity:"rate", relationName:"material_type" , mentionedEntity:"herbicide_name" , required: true } );

let plantAssetExampleAttributes = herbicideConvention.overlays.plant_asset.validExamples[0].attributes;
let plantAssetUUID = herbicideConvention.overlays.plant_asset.validExamples[0].id;

let herbicideConventionExample = {
    id: conventionUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    herbicide_log: {
        id: herbicideLogUUID,
        attributes: {
            name: "example herbicide log",
            status: "done",
        },
        relationships: {
            category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:logCategoryUUID
                }
            ] },
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: areaPercentageUUID
                },
                {
                    type: "quantity--material",
                    id: herbicideRateUUID
                }
            ] }
        },
    },
    log_category: logCategoryExample,
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    area_unit:  {
        id: percentageUnitUUID,
        attributes: {
            name: "%"
        },   
    },
    rate: {
        id: herbicideRateUUID,
        attributes: {
            label: "quantity",
            measure: "rate"
        },
        relationships: { 
            units: 
            { data: [
                {
                    type:"taxonomy_term--unit",
                    id: rateUnitUUID
                }
            ] },
            material_type:
            { data: [
                {
                    type:"taxonomy_term--material_type",
                    id: herbicideNameUUID
                }
            ] },
        }
    },
    rate_unit:  {
        id: rateUnitUUID,
        attributes: {
            //type: "taxonomy_term--unit",
            name: "cubic_ft"
        },   
    },
    herbicide_name: {
        id: herbicideNameUUID,
        attributes: {
            name: "aero"
        }
    }
};
let herbicideConventionError = {
    id: conventionUUID,
    type: "Object",
    plant_asset: {
        id: plantAssetUUID,
        attributes: plantAssetExampleAttributes
    },
    herbicide_log: {
        id: herbicideLogUUID,
        attributes: {
            name: "example herbicide log",
            status: "done",
        },
        relationships: {
            /*category: { data: [
                {
                    type: "taxonomy_term--log_category",
                    id:logCategoryUUID
                }
            ] },*/
            quantity: { data: [
                {
                    type: "quantity--standard",
                    id: areaPercentageUUID
                }
                /*{
                    type: "quantity--standard",
                    id: herbicideRateUUID
                }*/
            ] }
        },
    },
    //log_category: logCategoryExample,
    area_quantity: {
        id: areaPercentageUUID,
        attributes: {
            label: "area"
        },
        relationships: {
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: percentageUnitUUID
                }
            ] }
        }
    },
    area_unit:  {
        id: percentageUnitUUID,
        attributes: {
            name: "%"
        },   
    },
    rate: {
        id: herbicideRateUUID,
        attributes: {
            label: "quantity"
        },
        relationships: { 
            units: { data: [
                {
                    type:"taxonomy_term--unit",
                    id: rateUnitUUID
                }
            ] }
        }
    },
    rate_unit:  {
        id: rateUnitUUID,
        attributes: {
            name: "cubic_ft"
        },   
    },
};

herbicideConvention.validExamples = [herbicideConventionExample];
herbicideConvention.erroredExamples = [herbicideConventionError];

let test = herbicideConvention.testExamples();
let storageOperation = herbicideConvention.store();
