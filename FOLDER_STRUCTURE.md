# Folder Structure for the Repo

## Main Schema Folders

These folders follow a symetric structure organized around the idea of *inputs* and *outputs*.

* Output
  * collection.json
  * Collection
     * list of entitites //this would include conventions as an entity
  * Documentation
  * Validators
* Input
  * farmos.json //Raw FarmOS file monolithic
  * collection.json //seems duplicative, but I want to keep consistency between Input and Output.   This is just farmos.json de-drupalized...
  * Collection
     * list of entitites
  * Documentation


## Auxiliary Folders

### src

  This folder hosts the *nodejs* library `convention_builder`.

### dist

  Publication ready software. If an npm key is available, this will be published into the a node package.

### scripts

  This folder contains scripts that realize several tasks, such as publishing the schemata wiki and obtaining schemata from a farm, as well as rebuilding everything from scratch.

### experiments

  While researching json schema and how to use it, we had to create a lot of examples and code to experiment about its behaviour and meaning. We save all of this assuming on a minimum it has pedagogical value and could eventually be used for tutorials or further research if we find we need to further learn about *json schema*.

### test

  Contains two kinds of tests:

1. Unity tests for the `convention_builder` library.
2. A script, `conventions_against_examples`, that will test each compiled convention against the provided examples, ensuring all valid examples are approved and all invalid examples are rejected.
