// We test several stragegies to schematize relationships in an adequate way.
const { randomUUID } = require('crypto');
const {prepareDrupalSchemaForAJV, buildValidator} = require('./../src/schema_utilities.js');
const standaloneCode = require("ajv/dist/standalone").default;
const fs = require('fs');

let farmosSchemata = JSON.parse( fs.readFileSync(`${ __dirname }/../input/farmos.json`) );
let hyperSchema = JSON.parse( fs.readFileSync(`${ __dirname }/../src/hyper-schema.json`) );
// This is the same schema used in the `defineAndValidateConvention.js` example.

let conventionSchema = {
    $id: "/conventions/log/activity/tillage",
    title: "Tillage Log Convention",
    type: 'object',
    properties: {
        activity_log: {
            type: 'object',
            description: "Testing relationships",
            $ref: farmosSchemata.log.activity.$id,
            properties: {
                attributes: {
                    type: 'object',
                    properties: {
                        name: {
                            const: "tillage log"
                        }
                    }
                },
                relationships: {
                    type: 'object',
                    properties: {
                        quantity: {
                            type: "array",
                            minItems:2,
                            maxItems:2,
                            prefixItems: [
                                { type: "object",
                                    properties: {
                                        type: { const: "quantity--standard" },
                                        id: { const: { "$data": "/stir_quantity/id" } }
                                    }
                                },
                                { type: "object",
                                  properties: {
                                      type: { const: "quantity--standard" },
                                      id: { const: { "$data": "/depth_quantity/id" } }
                                  }
                                },
                            ],
                            // TODO
                            items: { type: "string" }
                        },
                        location: {
                            type: 'array',
                            minItems:1,
                        }
                    },
                    required: ['location']
                }
            },
            required: ['attributes', 'relationships']
        },
        stir_quantity: {
            type: 'object',
            description: "Must be labelled as *stir* and it's measure type is *ratio*. See documentation ADD LINK to get the standard specification for this ratio.",
            $ref: farmosSchemata.quantity.standard.$id,
            properties: {
                attributes: {
                    type: 'object',
                    properties: {
                        label: { const: "stir" },
                        measure: { const: "ratio" }
                    }
                }
            }
        },
        depth_quantity: {
            type: 'object',
            description: "Must be labelled as *depth* and it's measure type is *length*.",
            $ref: farmosSchemata.quantity.standard.$id,
            properties: {
                label: { const:"depth" },
                measure: { const: 'length' },
            },
        }
    },
    required: ['activity_log']
};

// testing the 'contains' keyword
let conventionSchemaContains = {
    $id: "/conventions/log/activity/tillage-cobntains",
    title: "Tillage Log Convention",
    type: 'object',
    properties: {
        activity_log: {
            type: 'object',
            description: "Testing relationships",
            $ref: farmosSchemata.log.activity.$id,
            properties: {
                attributes: {
                    type: 'object',
                    properties: {
                        name: {
                            const: "tillage log"
                        }
                    }
                },
                relationships: {
                    type: 'object',
                    properties: {
                        quantity: {
                            type: "array",
                            contains: {
                                oneOf: [
                                    { type: "object",
                                      properties: {
                                          type: { const: "quantity--standard" },
                                          id: { const: { "$data": "/stir_quantity/id" } }
                                      }
                                    },
                                    { type: "object",
                                      properties: {
                                          type: { const: "quantity--standard" },
                                          id: { const: { "$data": "/depth_quantity/id" } }
                                      }
                                    }
                                ]
                            },
                            minContains:2,
                            uniqueItems: true
                        },
                        location: {
                            type: 'array',
                            minItems:1,
                        }
                    },
                    required: ['location']
                }
            },
            required: ['attributes', 'relationships']
        },
        stir_quantity: {
            type: 'object',
            description: "Must be labelled as *stir* and it's measure type is *ratio*. See documentation ADD LINK to get the standard specification for this ratio.",
            $ref: farmosSchemata.quantity.standard.$id,
            properties: {
                attributes: {
                    type: 'object',
                    properties: {
                        label: { const: "stir" },
                        measure: { const: "ratio" }
                    }
                }
            }
        },
        depth_quantity: {
            type: 'object',
            description: "Must be labelled as *depth* and it's measure type is *length*.",
            $ref: farmosSchemata.quantity.standard.$id,
            properties: {
                label: { const:"depth" },
                measure: { const: 'length' },
            },
        }
    },
    required: ['activity_log']
};

let logUUID = randomUUID();
let stirUUID = randomUUID();
let depthUUID = randomUUID();

let example = {
    // each entity will be an attribute in the convention, with a reference to a known entity type.
    // we need more precission, for example constants.
    activity_log: {
        attributes: {
            status:'done',
            name: "tillage log"
        },
        relationships: {
            quantity: [
                {
                    type: "quantity--standard",
                    id: stirUUID
                },
                {
                    type: "quantity--standard",
                    id: depthUUID
                }
            ],
            location: [
                { type: "asset--land",
                  id: randomUUID()
                }
            ]
        },
        id: logUUID
    },
    stir_quantity: {
        attributes: {label: "stir"},
        id: stirUUID
    },
    depth_quantity: {
        attributes: {
            label:"depth"
        },
        id: depthUUID
    }
};


delete farmosSchemata.log.activity.$schema;
delete farmosSchemata.quantity.standard.$schema;

let validator = buildValidator({code: {source: true}});
validator.addSchema(farmosSchemata.log.activity);
validator.addSchema(farmosSchemata.quantity.standard);

let validate = validator.compile(conventionSchema);

// validate(exampleError);

let validateContainsRels = validator.compile(conventionSchemaContains);

// create two further examples modifying the one available
let exampleErrorMissingAQuantity = structuredClone(example);
let exampleExtraQuantity = structuredClone(example);

exampleErrorMissingAQuantity.activity_log.relationships.quantity = [ example.activity_log.relationships.quantity[1] ];
exampleExtraQuantity.activity_log.relationships.quantity.push({
    type: 'quantity--standard',
    id: '1bdf1008-824c-47af-86c4-229bcdf3628e'
});

// test examples with our validator
// the intended valid example is indeed valid
validateContainsRels(example);
// this example will be false, as it has less than the mandatory 2 contained items
validateContainsRels(exampleErrorMissingAQuantity);
validateContainsRels.errors[0].message;
// this example has more entities than prescribed. The new entity is freely assignable. It should succeed and it does.
validateContainsRels(exampleExtraQuantity);
