// In this example, we analyze how to use the or operator.
// The main intention is to allow free 'new' values when setting enums, by setting a free string as an alternative schema.
const { randomUUID } = require('crypto');
const {prepareDrupalSchemaForAJV, buildValidator} = require('../src/schema_utilities.js');
const standaloneCode = require("ajv/dist/standalone").default;
const fs = require('fs');

let basicExampleSchema = {
    '$id': "using_or_operator/basic",
    'type': 'object',
    'properties': {
        'number': { type: 'number' },
        'list_member': {
            'anyOf':
                [
                    { 'enum': ['rabbit', 'chicken', 'duck'] },
                    { type: 'string' }
                ]
        }
    }
};

// value belongs into the list
let validEnum = {
    number:1,
    list_member: 'rabbit'
};
// value falls back into the 'string' branch
let validString = {
    number:1,
    list_member: 'deer'
};
// value should not be accepted
let invalid = {
    number:'a',
    list_member: 10
};


let validatorObj = buildValidator();
let validatorFunction = validatorObj.compile(basicExampleSchema);

let testEnum = validatorFunction(validEnum);
let testString = validatorFunction(validString);
let testInvalid = validatorFunction(invalid);
validatorFunction.errors;
